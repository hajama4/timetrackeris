## Getting Started

### Clone the repository
```bash
$ git clone https://hajama4@bitbucket.org/hajama4/timetracker1.git
```

### Change directory
```bash
$ cd tracker1
```

### Use composer to manage and install dependencies
```bash
$ composer install
```

### Install the frontend dependencies
Run the command below from the project's root directory in another terminal:

```bash
$ yarn install
```

Do ensure that you have two separate terminals opened on your machine. One of them will be used to start the Symfony app while the other will keep the frontend running.

### Start the application
#### Backend
```bash
$ php bin/console server:run

My personaly used wampserver64 
```
Create database
```bash
Database name 'tracker'

```

To add database connection to your database got to .env
```bash
32 line DATABASE_URL=mysql://root:@127.0.0.1:3306/tracker?serverVersion=5.7.31
```
To launch database migrations
```bash

$ php my_project_name/bin/console doctrine:migrations:migrate
```
#### Frontend
```bash
 $ yarn run encore dev --watch
```
Also should change HomeComponent line 101 to your root directory
in feature it should be improved.
```bash
101: url: 'http://time1/my_project_name/public',
```