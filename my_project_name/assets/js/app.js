global.jQuery = require('jquery');
require('bootstrap-sass');
global.axios = require('axios');

import Vue from 'vue';

import VCalendar from 'v-calendar';

import Home from './components/HomeComponent';

new Vue({
    el: '#app',
    components: {Home}
});

Vue.use(VCalendar, {
    componentPrefix: 'vc'
});