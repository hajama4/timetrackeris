<?php

namespace App\Controller;

use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\DateTime;

class TaskController extends Controller
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $taskRepository;

    /**
     * ProjectController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->taskRepository = $entityManager->getRepository('App:Task');
    }

    /**
     * @Route("/tasks", name="task")
     */
    public function index()
    {
        $tasks = $this->taskRepository->findByUser($this->getUser()->getId());

        foreach($tasks as $task) {
            $task->setAddedDate(date_format($task->getAddedDate(), 'Y-m-d H:i:s'));
        }

        $jsonContent = $this->serializeObject($tasks);

        return new Response($jsonContent, Response::HTTP_OK);

    }

    /**
     * @Route("/downloadPdf", name="pdf")
     */
    public function downloadPdf(Request $request)
    {
        $tasks = $this->taskRepository->findByExampleField(
            $this->getUser()->getId(),
            $request->query->get('dateFrom'),
            $request->query->get('dateTo'),
        );

        foreach($tasks as $i => $task) {
            $tasks[$i]['addedDate'] = date_format($tasks[$i]['addedDate'], 'Y-m-d H:i:s');
        }

        $jsonContent = $this->serializeObject($tasks);

        return new Response($jsonContent, Response::HTTP_OK);

    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/tasks/create", name="create_task")
     */
    public function saveProjects(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        if($content['name']) {

            $task = new Task();
            $task->setUser($this->getUser());
            $task->setTitle($content['name']);
            $task->setComment($content['comment']);
            $task->setAddedDate(new \DateTime($content['added_date']));
            $task->setTimeSpent($content['time_spent']);
            $task->setCreatedAt(new \DateTime());
            $task->setUpdatedAt(new \DateTime());
            $this->updateDatabase($task);

            $task->setAddedDate(date_format($task->getAddedDate(), 'Y-m-d H:i:s'));

            // Serialize object into Json format
            $jsonContent = $this->serializeObject($task);

            return new Response($jsonContent, Response::HTTP_OK);
        }

        return new Response('Error', Response::HTTP_NOT_FOUND);

    }


    public function serializeObject($object)
    {
        $encoders = new JsonEncoder();
        $normalizers = new ObjectNormalizer();

        $normalizers->setCircularReferenceHandler(function ($obj) {
            return $obj->getId();
        });

        $serializer = new Serializer(array($normalizers), array($encoders));

        $jsonContent = $serializer->serialize($object, 'json');

        return $jsonContent;
    }


    public function updateDatabase($object)
    {
        $this->entityManager->persist($object);
        $this->entityManager->flush();
    }
}
